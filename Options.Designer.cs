﻿namespace TekMeter
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Options));
            this.apiKeyLabel = new System.Windows.Forms.Label();
            this.apiKeyTextBox = new System.Windows.Forms.TextBox();
            this.bandwidthLabel = new System.Windows.Forms.Label();
            this.bandwidthLimitTextBox = new System.Windows.Forms.TextBox();
            this.gbLabel = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.tekSavvyLinkLabel = new System.Windows.Forms.LinkLabel();
            this.getApiKeyAtLabel = new System.Windows.Forms.Label();
            this.showAcceptibleUsageCheckbox = new System.Windows.Forms.CheckBox();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.usageColorButton = new System.Windows.Forms.Button();
            this.backgroundColorButton = new System.Windows.Forms.Button();
            this.showWarningLine = new System.Windows.Forms.CheckBox();
            this.removeBackgroundColor = new System.Windows.Forms.Button();
            this.startToSysTrayCheckBox = new System.Windows.Forms.CheckBox();
            this.providerLabel = new System.Windows.Forms.Label();
            this.providerComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // apiKeyLabel
            // 
            this.apiKeyLabel.AutoSize = true;
            this.apiKeyLabel.Location = new System.Drawing.Point(14, 47);
            this.apiKeyLabel.Name = "apiKeyLabel";
            this.apiKeyLabel.Size = new System.Drawing.Size(46, 13);
            this.apiKeyLabel.TabIndex = 0;
            this.apiKeyLabel.Text = "Api Key:";
            // 
            // apiKeyTextBox
            // 
            this.apiKeyTextBox.Location = new System.Drawing.Point(81, 45);
            this.apiKeyTextBox.MaxLength = 100;
            this.apiKeyTextBox.Name = "apiKeyTextBox";
            this.apiKeyTextBox.Size = new System.Drawing.Size(338, 20);
            this.apiKeyTextBox.TabIndex = 3;
            // 
            // bandwidthLabel
            // 
            this.bandwidthLabel.AutoSize = true;
            this.bandwidthLabel.Location = new System.Drawing.Point(220, 20);
            this.bandwidthLabel.Name = "bandwidthLabel";
            this.bandwidthLabel.Size = new System.Drawing.Size(60, 13);
            this.bandwidthLabel.TabIndex = 2;
            this.bandwidthLabel.Text = "Bandwidth:";
            // 
            // bandwidthLimitTextBox
            // 
            this.bandwidthLimitTextBox.Location = new System.Drawing.Point(287, 18);
            this.bandwidthLimitTextBox.Name = "bandwidthLimitTextBox";
            this.bandwidthLimitTextBox.Size = new System.Drawing.Size(50, 20);
            this.bandwidthLimitTextBox.TabIndex = 2;
            // 
            // gbLabel
            // 
            this.gbLabel.AutoSize = true;
            this.gbLabel.Location = new System.Drawing.Point(337, 22);
            this.gbLabel.Name = "gbLabel";
            this.gbLabel.Size = new System.Drawing.Size(22, 13);
            this.gbLabel.TabIndex = 4;
            this.gbLabel.Text = "GB";
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(344, 294);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 11;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(263, 294);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 10;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // tekSavvyLinkLabel
            // 
            this.tekSavvyLinkLabel.AutoSize = true;
            this.tekSavvyLinkLabel.Location = new System.Drawing.Point(12, 310);
            this.tekSavvyLinkLabel.Name = "tekSavvyLinkLabel";
            this.tekSavvyLinkLabel.Size = new System.Drawing.Size(169, 13);
            this.tekSavvyLinkLabel.TabIndex = 7;
            this.tekSavvyLinkLabel.TabStop = true;
            this.tekSavvyLinkLabel.Text = "https://myaccount.teksavvy.com/";
            this.tekSavvyLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.tekSavvyLinkLabel_LinkClicked);
            // 
            // getApiKeyAtLabel
            // 
            this.getApiKeyAtLabel.AutoSize = true;
            this.getApiKeyAtLabel.Location = new System.Drawing.Point(12, 294);
            this.getApiKeyAtLabel.Name = "getApiKeyAtLabel";
            this.getApiKeyAtLabel.Size = new System.Drawing.Size(103, 13);
            this.getApiKeyAtLabel.TabIndex = 8;
            this.getApiKeyAtLabel.Text = "Get your API Key at:";
            // 
            // showAcceptibleUsageCheckbox
            // 
            this.showAcceptibleUsageCheckbox.AutoSize = true;
            this.showAcceptibleUsageCheckbox.Location = new System.Drawing.Point(152, 34);
            this.showAcceptibleUsageCheckbox.Name = "showAcceptibleUsageCheckbox";
            this.showAcceptibleUsageCheckbox.Size = new System.Drawing.Size(246, 17);
            this.showAcceptibleUsageCheckbox.TabIndex = 7;
            this.showAcceptibleUsageCheckbox.Text = "Show todays acceptible usage in sys tray icon.";
            this.showAcceptibleUsageCheckbox.UseVisualStyleBackColor = true;
            // 
            // usageColorButton
            // 
            this.usageColorButton.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usageColorButton.Location = new System.Drawing.Point(12, 34);
            this.usageColorButton.Name = "usageColorButton";
            this.usageColorButton.Size = new System.Drawing.Size(124, 23);
            this.usageColorButton.TabIndex = 4;
            this.usageColorButton.Text = "Usage Color";
            this.usageColorButton.UseVisualStyleBackColor = true;
            this.usageColorButton.Click += new System.EventHandler(this.usageColorButton_Click);
            // 
            // backgroundColorButton
            // 
            this.backgroundColorButton.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backgroundColorButton.Location = new System.Drawing.Point(12, 63);
            this.backgroundColorButton.Name = "backgroundColorButton";
            this.backgroundColorButton.Size = new System.Drawing.Size(124, 23);
            this.backgroundColorButton.TabIndex = 5;
            this.backgroundColorButton.Text = "Background Color";
            this.backgroundColorButton.UseVisualStyleBackColor = true;
            this.backgroundColorButton.Click += new System.EventHandler(this.backgroundColorButton_Click);
            // 
            // showWarningLine
            // 
            this.showWarningLine.Location = new System.Drawing.Point(152, 57);
            this.showWarningLine.Name = "showWarningLine";
            this.showWarningLine.Size = new System.Drawing.Size(217, 36);
            this.showWarningLine.TabIndex = 8;
            this.showWarningLine.Text = "Show a warning line on the sys tray icon at 75 and 90 percent usage.";
            this.showWarningLine.UseVisualStyleBackColor = true;
            // 
            // removeBackgroundColor
            // 
            this.removeBackgroundColor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeBackgroundColor.Location = new System.Drawing.Point(12, 93);
            this.removeBackgroundColor.Name = "removeBackgroundColor";
            this.removeBackgroundColor.Size = new System.Drawing.Size(124, 23);
            this.removeBackgroundColor.TabIndex = 6;
            this.removeBackgroundColor.Text = "Remove Background Color";
            this.removeBackgroundColor.UseVisualStyleBackColor = true;
            this.removeBackgroundColor.Click += new System.EventHandler(this.removeBackgroundColor_Click);
            // 
            // startToSysTrayCheckBox
            // 
            this.startToSysTrayCheckBox.Location = new System.Drawing.Point(152, 93);
            this.startToSysTrayCheckBox.Name = "startToSysTrayCheckBox";
            this.startToSysTrayCheckBox.Size = new System.Drawing.Size(217, 35);
            this.startToSysTrayCheckBox.TabIndex = 9;
            this.startToSysTrayCheckBox.Text = "Hide application window on start (only show sys tray icon).";
            this.startToSysTrayCheckBox.UseVisualStyleBackColor = true;
            // 
            // providerLabel
            // 
            this.providerLabel.AutoSize = true;
            this.providerLabel.Location = new System.Drawing.Point(14, 20);
            this.providerLabel.Name = "providerLabel";
            this.providerLabel.Size = new System.Drawing.Size(49, 13);
            this.providerLabel.TabIndex = 16;
            this.providerLabel.Text = "Provider:";
            // 
            // providerComboBox
            // 
            this.providerComboBox.DisplayMember = "Name";
            this.providerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.providerComboBox.FormattingEnabled = true;
            this.providerComboBox.Location = new System.Drawing.Point(81, 17);
            this.providerComboBox.Name = "providerComboBox";
            this.providerComboBox.Size = new System.Drawing.Size(121, 21);
            this.providerComboBox.TabIndex = 1;
            this.providerComboBox.ValueMember = "ID";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.showWarningLine);
            this.groupBox1.Controls.Add(this.showAcceptibleUsageCheckbox);
            this.groupBox1.Controls.Add(this.usageColorButton);
            this.groupBox1.Controls.Add(this.startToSysTrayCheckBox);
            this.groupBox1.Controls.Add(this.backgroundColorButton);
            this.groupBox1.Controls.Add(this.removeBackgroundColor);
            this.groupBox1.Location = new System.Drawing.Point(17, 122);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(402, 145);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sys Tray Icon Options";
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 329);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.providerComboBox);
            this.Controls.Add(this.providerLabel);
            this.Controls.Add(this.getApiKeyAtLabel);
            this.Controls.Add(this.tekSavvyLinkLabel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.gbLabel);
            this.Controls.Add(this.bandwidthLimitTextBox);
            this.Controls.Add(this.bandwidthLabel);
            this.Controls.Add(this.apiKeyTextBox);
            this.Controls.Add(this.apiKeyLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Options";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Options_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label apiKeyLabel;
        private System.Windows.Forms.TextBox apiKeyTextBox;
        private System.Windows.Forms.Label bandwidthLabel;
        private System.Windows.Forms.TextBox bandwidthLimitTextBox;
        private System.Windows.Forms.Label gbLabel;
        private System.Windows.Forms.Button saveButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.LinkLabel tekSavvyLinkLabel;
        private System.Windows.Forms.Label getApiKeyAtLabel;
        private System.Windows.Forms.CheckBox showAcceptibleUsageCheckbox;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button usageColorButton;
        private System.Windows.Forms.Button backgroundColorButton;
        private System.Windows.Forms.CheckBox showWarningLine;
        private System.Windows.Forms.Button removeBackgroundColor;
        private System.Windows.Forms.CheckBox startToSysTrayCheckBox;
        private System.Windows.Forms.Label providerLabel;
        private System.Windows.Forms.ComboBox providerComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}