﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TekMeter.Services;
using TekMeter.Services.TekSavvy;

namespace TekMeter
{
    public partial class TekMeter : Form
    {
        private SysTrayGraphics sysTray;
        private bool firstRunWithHide = false;

        public TekMeter()
        {
            InitializeComponent();
            InitializeSysTray(0, -1);
            firstRunWithHide = Properties.Settings.Default.StartToSysTray; // grumble...
            usageUpdateTimer.Start();
        }

        private void InitializeSysTray(double first, double second)
        {
            sysTray = new SysTrayGraphics(sysTrayIcon);

            sysTray.foreColour = Properties.Settings.Default.UsageColor;
            sysTray.backColour = Properties.Settings.Default.BackgroundColor;
            sysTray.transparent = Properties.Settings.Default.BackgroundColor == Color.Transparent;

            sysTray.InitializeSysTrayIcon();

            sysTray.UpdateSysTrayIconValues(first, second, Properties.Settings.Default.ShowWarningLine);
        }

        private void RefreshDisplay()
        {
            // Refresh elements
            refreshProgressBars();
            refreshSysTrayIconTooltip();
            resfreshDates();

            // Calculate statistics
            int numOfDaysRemainingInMonth;
            double gbsPerDayAmount, todaysAcceptableUsage;
            calculateStatistics(out numOfDaysRemainingInMonth, out gbsPerDayAmount, out todaysAcceptableUsage);

            // Set the usage numbers and statistics on the labels.
            refreshUsageLabels(gbsPerDayAmount, numOfDaysRemainingInMonth);

            // Re-init the sys try to show the new info.
            InitializeSysTray(Properties.Settings.Default.OnPeakDownload, todaysAcceptableUsage);
        }

        private void calculateStatistics(out int numOfDaysRemainingInMonth, out double gbsPerDayAmount, out double todaysAcceptableUsage)
        {
            DateTime currentDate = DateTime.Now;
            int numOfDaysInMonth = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);
            numOfDaysRemainingInMonth = numOfDaysInMonth - (currentDate.Day - 1); // TODO do we subtract a day here for non-teksavvy providers?
            double remainingBw = Properties.Settings.Default.BandwidthLimit - Properties.Settings.Default.OnPeakDownload;
            gbsPerDayAmount = Math.Round(remainingBw / numOfDaysRemainingInMonth, MidpointRounding.AwayFromZero);

            if (gbsPerDayAmount < 0)
            {
                gbsPerDayAmount = 0;
            }

            // Acceptible usage per day in sys tray
            todaysAcceptableUsage = -1;
            if (Properties.Settings.Default.ShowAcceptUsageTray)
            {
                todaysAcceptableUsage = gbsPerDayAmount;
            }
        }

        private void resfreshDates()
        {
            lastUpdateDateTimeLabel.Text = Properties.Settings.Default.LastRefreshTime.ToString();
            fromDateLabel.Text = Properties.Settings.Default.UsageStartDate.ToString("yyyy-MM-dd");
            toDateLabel.Text = Properties.Settings.Default.UsageEndDate.ToString("yyyy-MM-dd");
        }

        private void refreshUsageLabels(double gbsPerDayAmount, int numOfDaysRemainingInMonth)
        {
            onPeakDownloadAmountLabel.Text = Properties.Settings.Default.OnPeakDownload.ToString() + " GB";
            onPeakUploadAmountLabel.Text = Properties.Settings.Default.OnPeakUpload.ToString() + " GB";

            offPeakDownloadAmountLabel.Text = Properties.Settings.Default.OffPeakDownload.ToString() + " GB";
            offPeakUploadAmountLabel.Text = Properties.Settings.Default.OffPeakUpload.ToString() + " GB";

            gbPerDayLabel.Text = "You can use " + gbsPerDayAmount + " GBs a day for the next " + numOfDaysRemainingInMonth + " days.";
        }

        private void refreshSysTrayIconTooltip()
        {
            sysTrayIcon.Text = "Down: " + Properties.Settings.Default.OnPeakDownload.ToString() + " GB\n";
            sysTrayIcon.Text = sysTrayIcon.Text + "Up: " + Properties.Settings.Default.OnPeakUpload.ToString() + " GB";
        }

        private void refreshProgressBars()
        {
            if (Properties.Settings.Default.BandwidthLimit > 0)
            {
                int roundedOnPeakDownload = Convert.ToInt16(Math.Round(Properties.Settings.Default.OnPeakDownload, MidpointRounding.AwayFromZero));
                int bandwidthLimit = Properties.Settings.Default.BandwidthLimit;

                // If we are over the limit. Lets not break the progress bar max.
                if (roundedOnPeakDownload > bandwidthLimit)
                {
                    roundedOnPeakDownload = bandwidthLimit;
                }

                bandwidthUsageProgressBar.Maximum = Properties.Settings.Default.BandwidthLimit;
                bandwidthUsageProgressBar.Value = roundedOnPeakDownload;
            }

            // Month progress bar
            DateTime currentDate = DateTime.Now;
            int numOfDaysInMonth = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);
            monthProgressBar.Maximum = numOfDaysInMonth;
            monthProgressBar.Value = currentDate.Day - 1;
        }

        private void retrieveDataUsage(bool force)
        {
            IUsageService service = GetUsageService();

            try
            {
                if (service != null && (Properties.Settings.Default.AutomaticUpdating && service.CanUpdate() || force))
                {
                    writeUsageData(service.GetUsage());
                }

                RefreshDisplay();
            }
            catch (Exception e)
            {
                DisplayError(force, service, e);
            }
        }

        private IList<UsageHistory> retrieveHistory()
        {
            IUsageService service = GetUsageService();

            try
            {
                if (service != null)
                {
                    return service.GetUsageHistory();
                }
            }
            catch (Exception e)
            {
                DisplayError(true, service, e);
            }

            return new List<UsageHistory>();
        }

        private void DisplayError(bool force, IUsageService service, Exception e)
        {
            if (force)
            {
                MessageBox.Show(
                    "Unable to retrieve usage data from " + service.GetProviderName() + ".\n\nGuru Meditation:\n(" + e.Message + ")",
                    "TekMeter",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            else
            {
                sysTrayIcon.ShowBalloonTip(
                    5000,
                    "TekMeter - Something went wrong...",
                    "Unable to retrive usage data from " + service.GetProviderName() + ".\n(" + e.Message + ")",
                    ToolTipIcon.Error);
            }
        }

        private static IUsageService GetUsageService()
        {
            switch (Properties.Settings.Default.ServiceProvider)
            {
                case 1: // TekSavvy
                    return new TekSavvyUsageService(Properties.Settings.Default.ApiKey);
                default: break; // do nothing for now.
            }

            return null;
        }

        private void writeUsageData(Usage usage)
        {
            Properties.Settings.Default.LastRefreshTime = DateTime.Now;

            Properties.Settings.Default.UsageStartDate = usage.StartDate;
            Properties.Settings.Default.UsageEndDate = usage.EndDate;

            Properties.Settings.Default.OnPeakDownload = usage.OnPeakDownload;
            Properties.Settings.Default.OnPeakUpload = usage.OnPeakUpload;

            Properties.Settings.Default.OffPeakDownload = usage.OffPeakDownload;
            Properties.Settings.Default.OffPeakUpload = usage.OffPeakUpload;

            Properties.Settings.Default.Save();
        }

        /****************************** *
         * METHODS CALLED BY COMPONENTS *
         ********************************/

        private void TekMeter_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        /*
         * This is called by the timer.
         */
        private void refreshDataUsage(object sender, EventArgs e)
        {
            retrieveDataUsage(false);
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            retrieveDataUsage(true);
        }

        private void sysTrayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.Visible && !firstRunWithHide)
            {
                this.Hide();
            }
            else
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
                this.Activate();
                firstRunWithHide = false; // grumble...
            }
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Options options = new Options();
            options.ShowDialog();

            retrieveDataUsage(false);
            RefreshDisplay();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            usageUpdateTimer.Stop();
            Application.Exit();
        }

        private void autoUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            autoUpdateToolStripMenuItem.Checked = !autoUpdateToolStripMenuItem.Checked;
            Properties.Settings.Default.AutomaticUpdating = autoUpdateToolStripMenuItem.Checked;
            Properties.Settings.Default.Save();
        }

        private void historyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            History history = new History(retrieveHistory());
            history.Show();
        }

        private void TekMeter_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.StartToSysTray)
            {
                this.ShowInTaskbar = false;
                this.Hide();
            }

            autoUpdateToolStripMenuItem.Checked = Properties.Settings.Default.AutomaticUpdating;

            if (string.IsNullOrEmpty(Properties.Settings.Default.ApiKey))
            {
                MessageBox.Show(
                    "You must configure your service provider information in the settings.",
                    "TekMeter",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                Options options = new Options();
                options.ShowDialog();
            }

            retrieveDataUsage(false);
            RefreshDisplay();
        }
    }
}