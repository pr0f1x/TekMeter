﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using TekMeter.Services;

namespace TekMeter
{
    public partial class Options : Form
    {
        public Options()
        {
            InitializeComponent();
        }

        private void Options_Load(object sender, EventArgs e)
        {
            apiKeyTextBox.Text = Properties.Settings.Default.ApiKey;
            bandwidthLimitTextBox.Text = Properties.Settings.Default.BandwidthLimit.ToString();
            
            usageColorButton.BackColor = Properties.Settings.Default.UsageColor;
            backgroundColorButton.BackColor = Properties.Settings.Default.BackgroundColor;

            showAcceptibleUsageCheckbox.Checked = Properties.Settings.Default.ShowAcceptUsageTray;
            showWarningLine.Checked = Properties.Settings.Default.ShowWarningLine;
            startToSysTrayCheckBox.Checked = Properties.Settings.Default.StartToSysTray;

            providerComboBox.DataSource = new ServiceProvider[] {
                new ServiceProvider{ ID = 1, Name = "TekSavvy" }
            };

            providerComboBox.SelectedValue = Properties.Settings.Default.ServiceProvider;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.ApiKey = apiKeyTextBox.Text;

            if (IsNumeric(bandwidthLimitTextBox.Text)) {
                int bwLimit = int.Parse(bandwidthLimitTextBox.Text);
                if (bwLimit > 0)
                {
                    Properties.Settings.Default.BandwidthLimit = int.Parse(bandwidthLimitTextBox.Text);
                } else
                {
                    Properties.Settings.Default.BandwidthLimit = 0;
                }
            } else {
                Properties.Settings.Default.BandwidthLimit = 0;
            }

            Properties.Settings.Default.UsageColor = usageColorButton.BackColor;
            Properties.Settings.Default.BackgroundColor = backgroundColorButton.BackColor;

            Properties.Settings.Default.ShowAcceptUsageTray = showAcceptibleUsageCheckbox.Checked;
            Properties.Settings.Default.ShowWarningLine = showWarningLine.Checked;
            Properties.Settings.Default.StartToSysTray = startToSysTrayCheckBox.Checked;
            Properties.Settings.Default.ServiceProvider = (int)providerComboBox.SelectedValue;

            Properties.Settings.Default.Save();

            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void usageColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                usageColorButton.BackColor = colorDialog.Color;
            }
        }

        private void backgroundColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                backgroundColorButton.BackColor = colorDialog.Color;
            }
        }

        private void removeBackgroundColor_Click(object sender, EventArgs e)
        {
            backgroundColorButton.BackColor = Color.Transparent;
        }

        private void tekSavvyLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://myaccount.teksavvy.com/");
        }

        public static System.Boolean IsNumeric(System.Object Expression)
        {
            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                if (Expression is string)
                    Double.Parse(Expression as string);
                else
                    Double.Parse(Expression.ToString());
                return true;
            }
            catch { } // just dismiss errors but return false
            return false;
        }
    }
}
