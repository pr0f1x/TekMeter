# TekMeter

Use it to pull your bandwidth usage information from TekSavvy and display it in 
a clean lightweight window and systray icon.

Inspired by CapSavvy by Guspaz

![tekMeter](/uploads/a6015ed722a1f5732ec9746689a09c23/tekMeter.png)

Download:
 - [TekMeter-1.1.1.zip](/uploads/c405f0566f8f8e751f43963951a140f4/TekMeter-1.1.1.zip)