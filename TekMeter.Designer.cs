﻿namespace TekMeter
{
    partial class TekMeter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                sysTray.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TekMeter));
            this.sysTrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bandwidthUsageProgressBar = new System.Windows.Forms.ProgressBar();
            this.bandwidthUsageLabel = new System.Windows.Forms.Label();
            this.offPeakDownloadAmountLabel = new System.Windows.Forms.Label();
            this.offPeakUploadAmountLabel = new System.Windows.Forms.Label();
            this.onPeakUploadAmountLabel = new System.Windows.Forms.Label();
            this.onPeakDownloadAmountLabel = new System.Windows.Forms.Label();
            this.lastUpdateDateTimeLabel = new System.Windows.Forms.Label();
            this.toDateLabel = new System.Windows.Forms.Label();
            this.fromDateLabel = new System.Windows.Forms.Label();
            this.usageUpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.toLabel = new System.Windows.Forms.Label();
            this.monthProgressBar = new System.Windows.Forms.ProgressBar();
            this.gbPerDayLabel = new System.Windows.Forms.Label();
            this.OnPeakUsageLabel = new System.Windows.Forms.Label();
            this.offPeakUsageLabel = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.historyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // sysTrayIcon
            // 
            this.sysTrayIcon.Text = "N/A";
            this.sysTrayIcon.Visible = true;
            this.sysTrayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.sysTrayIcon_MouseDoubleClick);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(305, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.historyToolStripMenuItem,
            this.autoUpdateToolStripMenuItem,
            this.refreshToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.settingsToolStripMenuItem.Text = "Settings...";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // autoUpdateToolStripMenuItem
            // 
            this.autoUpdateToolStripMenuItem.Checked = true;
            this.autoUpdateToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoUpdateToolStripMenuItem.Name = "autoUpdateToolStripMenuItem";
            this.autoUpdateToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.autoUpdateToolStripMenuItem.Text = "Auto Refresh";
            this.autoUpdateToolStripMenuItem.Click += new System.EventHandler(this.autoUpdateToolStripMenuItem_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // bandwidthUsageProgressBar
            // 
            this.bandwidthUsageProgressBar.Location = new System.Drawing.Point(13, 51);
            this.bandwidthUsageProgressBar.Name = "bandwidthUsageProgressBar";
            this.bandwidthUsageProgressBar.Size = new System.Drawing.Size(280, 23);
            this.bandwidthUsageProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.bandwidthUsageProgressBar.TabIndex = 2;
            // 
            // bandwidthUsageLabel
            // 
            this.bandwidthUsageLabel.AutoSize = true;
            this.bandwidthUsageLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandwidthUsageLabel.Location = new System.Drawing.Point(12, 32);
            this.bandwidthUsageLabel.Name = "bandwidthUsageLabel";
            this.bandwidthUsageLabel.Size = new System.Drawing.Size(104, 13);
            this.bandwidthUsageLabel.TabIndex = 3;
            this.bandwidthUsageLabel.Text = "BANDWIDTH USAGE";
            // 
            // offPeakDownloadAmountLabel
            // 
            this.offPeakDownloadAmountLabel.AutoSize = true;
            this.offPeakDownloadAmountLabel.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.offPeakDownloadAmountLabel.Location = new System.Drawing.Point(158, 125);
            this.offPeakDownloadAmountLabel.Name = "offPeakDownloadAmountLabel";
            this.offPeakDownloadAmountLabel.Size = new System.Drawing.Size(64, 29);
            this.offPeakDownloadAmountLabel.TabIndex = 8;
            this.offPeakDownloadAmountLabel.Text = "0 GB";
            // 
            // offPeakUploadAmountLabel
            // 
            this.offPeakUploadAmountLabel.AutoSize = true;
            this.offPeakUploadAmountLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.offPeakUploadAmountLabel.Location = new System.Drawing.Point(161, 160);
            this.offPeakUploadAmountLabel.Name = "offPeakUploadAmountLabel";
            this.offPeakUploadAmountLabel.Size = new System.Drawing.Size(34, 16);
            this.offPeakUploadAmountLabel.TabIndex = 9;
            this.offPeakUploadAmountLabel.Text = "0 GB";
            // 
            // onPeakUploadAmountLabel
            // 
            this.onPeakUploadAmountLabel.AutoSize = true;
            this.onPeakUploadAmountLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.onPeakUploadAmountLabel.Location = new System.Drawing.Point(15, 160);
            this.onPeakUploadAmountLabel.Name = "onPeakUploadAmountLabel";
            this.onPeakUploadAmountLabel.Size = new System.Drawing.Size(34, 16);
            this.onPeakUploadAmountLabel.TabIndex = 7;
            this.onPeakUploadAmountLabel.Text = "0 GB";
            // 
            // onPeakDownloadAmountLabel
            // 
            this.onPeakDownloadAmountLabel.AutoSize = true;
            this.onPeakDownloadAmountLabel.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.onPeakDownloadAmountLabel.Location = new System.Drawing.Point(12, 125);
            this.onPeakDownloadAmountLabel.Name = "onPeakDownloadAmountLabel";
            this.onPeakDownloadAmountLabel.Size = new System.Drawing.Size(64, 29);
            this.onPeakDownloadAmountLabel.TabIndex = 6;
            this.onPeakDownloadAmountLabel.Text = "0 GB";
            // 
            // lastUpdateDateTimeLabel
            // 
            this.lastUpdateDateTimeLabel.AutoSize = true;
            this.lastUpdateDateTimeLabel.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastUpdateDateTimeLabel.Location = new System.Drawing.Point(213, 232);
            this.lastUpdateDateTimeLabel.Name = "lastUpdateDateTimeLabel";
            this.lastUpdateDateTimeLabel.Size = new System.Drawing.Size(90, 10);
            this.lastUpdateDateTimeLabel.TabIndex = 12;
            this.lastUpdateDateTimeLabel.Text = "0000-00-00 00:00:00 AM";
            // 
            // toDateLabel
            // 
            this.toDateLabel.AutoSize = true;
            this.toDateLabel.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toDateLabel.Location = new System.Drawing.Point(244, 34);
            this.toDateLabel.Name = "toDateLabel";
            this.toDateLabel.Size = new System.Drawing.Size(51, 11);
            this.toDateLabel.TabIndex = 16;
            this.toDateLabel.Text = "0000-00-00";
            // 
            // fromDateLabel
            // 
            this.fromDateLabel.AutoSize = true;
            this.fromDateLabel.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromDateLabel.Location = new System.Drawing.Point(171, 34);
            this.fromDateLabel.Name = "fromDateLabel";
            this.fromDateLabel.Size = new System.Drawing.Size(51, 11);
            this.fromDateLabel.TabIndex = 14;
            this.fromDateLabel.Text = "0000-00-00";
            // 
            // usageUpdateTimer
            // 
            this.usageUpdateTimer.Interval = 3600000;
            this.usageUpdateTimer.Tick += new System.EventHandler(this.refreshDataUsage);
            // 
            // toLabel
            // 
            this.toLabel.AutoSize = true;
            this.toLabel.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toLabel.Location = new System.Drawing.Point(229, 34);
            this.toLabel.Name = "toLabel";
            this.toLabel.Size = new System.Drawing.Size(8, 11);
            this.toLabel.TabIndex = 17;
            this.toLabel.Text = "-";
            // 
            // monthProgressBar
            // 
            this.monthProgressBar.Location = new System.Drawing.Point(13, 76);
            this.monthProgressBar.Name = "monthProgressBar";
            this.monthProgressBar.Size = new System.Drawing.Size(280, 10);
            this.monthProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.monthProgressBar.TabIndex = 18;
            // 
            // gbPerDayLabel
            // 
            this.gbPerDayLabel.AutoSize = true;
            this.gbPerDayLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPerDayLabel.Location = new System.Drawing.Point(15, 196);
            this.gbPerDayLabel.Name = "gbPerDayLabel";
            this.gbPerDayLabel.Size = new System.Drawing.Size(225, 13);
            this.gbPerDayLabel.TabIndex = 10;
            this.gbPerDayLabel.Text = "You can use 0 GBs a day for the next 0 days.";
            // 
            // OnPeakUsageLabel
            // 
            this.OnPeakUsageLabel.AutoSize = true;
            this.OnPeakUsageLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OnPeakUsageLabel.Location = new System.Drawing.Point(15, 112);
            this.OnPeakUsageLabel.Name = "OnPeakUsageLabel";
            this.OnPeakUsageLabel.Size = new System.Drawing.Size(86, 13);
            this.OnPeakUsageLabel.TabIndex = 19;
            this.OnPeakUsageLabel.Text = "ON PEAK USAGE";
            // 
            // offPeakUsageLabel
            // 
            this.offPeakUsageLabel.AutoSize = true;
            this.offPeakUsageLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.offPeakUsageLabel.Location = new System.Drawing.Point(161, 112);
            this.offPeakUsageLabel.Name = "offPeakUsageLabel";
            this.offPeakUsageLabel.Size = new System.Drawing.Size(91, 13);
            this.offPeakUsageLabel.TabIndex = 20;
            this.offPeakUsageLabel.Text = "OFF PEAK USAGE";
            // 
            // historyToolStripMenuItem
            // 
            this.historyToolStripMenuItem.Name = "historyToolStripMenuItem";
            this.historyToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.historyToolStripMenuItem.Text = "History...";
            this.historyToolStripMenuItem.Click += new System.EventHandler(this.historyToolStripMenuItem_Click);
            // 
            // TekMeter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 244);
            this.Controls.Add(this.offPeakUsageLabel);
            this.Controls.Add(this.OnPeakUsageLabel);
            this.Controls.Add(this.gbPerDayLabel);
            this.Controls.Add(this.lastUpdateDateTimeLabel);
            this.Controls.Add(this.offPeakDownloadAmountLabel);
            this.Controls.Add(this.onPeakUploadAmountLabel);
            this.Controls.Add(this.offPeakUploadAmountLabel);
            this.Controls.Add(this.monthProgressBar);
            this.Controls.Add(this.toLabel);
            this.Controls.Add(this.toDateLabel);
            this.Controls.Add(this.onPeakDownloadAmountLabel);
            this.Controls.Add(this.bandwidthUsageLabel);
            this.Controls.Add(this.fromDateLabel);
            this.Controls.Add(this.bandwidthUsageProgressBar);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TekMeter";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TekMeter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TekMeter_FormClosing);
            this.Load += new System.EventHandler(this.TekMeter_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NotifyIcon sysTrayIcon;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ProgressBar bandwidthUsageProgressBar;
        private System.Windows.Forms.Label bandwidthUsageLabel;
        private System.Windows.Forms.Label offPeakUploadAmountLabel;
        private System.Windows.Forms.Label offPeakDownloadAmountLabel;
        private System.Windows.Forms.Label onPeakUploadAmountLabel;
        private System.Windows.Forms.Label onPeakDownloadAmountLabel;
        private System.Windows.Forms.Timer usageUpdateTimer;
        private System.Windows.Forms.Label toDateLabel;
        private System.Windows.Forms.Label fromDateLabel;
        private System.Windows.Forms.Label lastUpdateDateTimeLabel;
        private System.Windows.Forms.Label toLabel;
        private System.Windows.Forms.ProgressBar monthProgressBar;
        private System.Windows.Forms.Label gbPerDayLabel;
        private System.Windows.Forms.Label OnPeakUsageLabel;
        private System.Windows.Forms.Label offPeakUsageLabel;
        private System.Windows.Forms.ToolStripMenuItem autoUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem historyToolStripMenuItem;
    }
}

