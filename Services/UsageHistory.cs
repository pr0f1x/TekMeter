﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TekMeter.Services
{
    public class UsageHistory
    {
        public DateTime Date { get; set; }
        public double OnPeakDownload { get; set; }
        public double OnPeakUpload { get; set; }
        public double OffPeakDownload { get; set; }
        public double OffPeakUpload { get; set; }
    }
}
