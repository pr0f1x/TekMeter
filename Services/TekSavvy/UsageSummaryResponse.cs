﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TekMeter.Services.TekSavvy
{

    [DataContract]
    class UsageSummaryResponse
    {
        [DataMember]
        public List<UsageSummaryValue> value { get; set; }
    }
}
