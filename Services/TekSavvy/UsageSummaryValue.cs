﻿using System.Runtime.Serialization;

namespace TekMeter.Services.TekSavvy
{
    [DataContract]
    class UsageSummaryValue
    {
        [DataMember]
        public string StartDate { get; set; }

        [DataMember]
        public string EndDate { get; set; }

        [DataMember]
        public string OID { get; set; }

        [DataMember]
        public bool IsCurrent { get; set; }

        [DataMember]
        public double OnPeakDownload { get; set; }

        [DataMember]
        public double OnPeakUpload { get; set; }

        [DataMember]
        public double OffPeakDownload { get; set; }

        [DataMember]
        public double OffPeakUpload { get; set; }
    }
}
