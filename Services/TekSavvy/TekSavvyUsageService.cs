﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;

namespace TekMeter.Services.TekSavvy
{
    class TekSavvyUsageService : IUsageService
    {
        private string apiKey;

        public TekSavvyUsageService(string apiKey)
        {
            this.apiKey = apiKey;
        }

        public Usage GetUsage()
        {
            WebResponse webResponse = CallUsageSummaryWebService(this.apiKey);
            Usage usage = new Usage();

            using (var stream = webResponse.GetResponseStream())
            {
                PopulateUsageSummaryWithResponse(SerializeUsageSummaryResponse(webResponse.GetResponseStream()), usage);
            }

            return usage;
        }

        public IList<UsageHistory> GetUsageHistory()
        {
            IList<UsageHistory> usageHistoryList = new List<UsageHistory>();
            WebResponse webResponse = CallUsageRecordsWebService(this.apiKey);

            using (var stream = webResponse.GetResponseStream())
            {
                PopulateUsageHistoryWithResponse(SerializeUsageRecordsResponse(webResponse.GetResponseStream()), usageHistoryList);
            }

            return usageHistoryList;
        }

        private void PopulateUsageHistoryWithResponse(UsageRecordsResponse response, IList<UsageHistory> usageHistoryList)
        {
            foreach ( UsageRecordsValue value in response.value )
            {
                UsageHistory usageHistory = new UsageHistory();

                DateTime parsedDate;
                DateTime.TryParseExact(value.Date,
                        "yyyy-MM-ddTHH:mm:ss", null,
                        DateTimeStyles.None, out parsedDate);
                usageHistory.Date = parsedDate;

                usageHistory.OnPeakDownload = value.OnPeakDownload;
                usageHistory.OffPeakDownload = value.OffPeakDownload;

                usageHistory.OnPeakUpload = value.OnPeakUpload;
                usageHistory.OffPeakUpload = value.OffPeakUpload;

                usageHistoryList.Add(usageHistory);
            }
        }

        private void PopulateUsageSummaryWithResponse(UsageSummaryResponse response, Usage usage)
        {
            // Start Date Example: 2016-05-01T00:00:00
            DateTime parsedStartDate;
            DateTime.TryParseExact(response.value[0].StartDate,
                    "yyyy-MM-ddTHH:mm:ss", null,
                    DateTimeStyles.None, out parsedStartDate);
            usage.StartDate = parsedStartDate;

            // End Date Example: 2016-05-26T00:00:00
            DateTime parsedEndDate;
            DateTime.TryParseExact(response.value[0].EndDate,
                    "yyyy-MM-ddTHH:mm:ss", null,
                    DateTimeStyles.None, out parsedEndDate);

            // We subract one day here because TekSavvy does not include 
            // the usage for the current day in their stats but for some 
            // reason sets the end date as the current day.
            usage.EndDate = parsedEndDate.AddDays(-1);

            usage.OnPeakDownload = response.value[0].OnPeakDownload;
            usage.OffPeakDownload = response.value[0].OffPeakDownload;

            usage.OnPeakUpload = response.value[0].OnPeakUpload;
            usage.OffPeakUpload = response.value[0].OffPeakUpload;
        }

        private UsageRecordsResponse SerializeUsageRecordsResponse(Stream responseStream)
        {
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(UsageRecordsResponse));
            UsageRecordsResponse response = (UsageRecordsResponse)jsonSerializer.ReadObject(responseStream);
            return response;
        }

        private UsageSummaryResponse SerializeUsageSummaryResponse(Stream responseStream)
        {
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(UsageSummaryResponse));
            UsageSummaryResponse response = (UsageSummaryResponse)jsonSerializer.ReadObject(responseStream);
            return response;
        }

        private WebResponse CallUsageSummaryWebService(string apiKey)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            WebRequest req = HttpWebRequest.Create("https://api.teksavvy.com/web/Usage/UsageSummaryRecords?$filter=IsCurrent%20eq%20true");
            ((HttpWebRequest)req).Headers.Add("TekSavvy-APIKey", apiKey);

            return req.GetResponse();
        }

        private WebResponse CallUsageRecordsWebService(string apiKey)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            WebRequest req = HttpWebRequest.Create("https://api.teksavvy.com/web/Usage/UsageRecords?$skip=40");
            ((HttpWebRequest)req).Headers.Add("TekSavvy-APIKey", apiKey);

            return req.GetResponse();
        }

        public bool CanUpdate()
        {
            // Since TekSavvy only updates their usage data once per day we
            // do not need to actually call the service every hour. We can 
            // just check once per day in the morning some time.
            DateTime lastRefreshTime = Properties.Settings.Default.LastRefreshTime;
            lastRefreshTime = new DateTime(lastRefreshTime.Year, lastRefreshTime.Month, lastRefreshTime.Day, 5, 0, 0).AddDays(1); // Only check after 5am.

            return DateTime.Now >= lastRefreshTime;
        }

        public string GetProviderName()
        {
            return "TekSavvy";
        }
    }
}
