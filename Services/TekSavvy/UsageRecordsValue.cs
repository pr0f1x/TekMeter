﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TekMeter.Services.TekSavvy
{
    [DataContract]
    class UsageRecordsValue
    {
        [DataMember]
        public string Date { get; set; }

        [DataMember]
        public string OID { get; set; }

        [DataMember]
        public float OnPeakDownload { get; set; }

        [DataMember]
        public float OnPeakUpload { get; set; }

        [DataMember]
        public float OffPeakDownload { get; set; }

        [DataMember]
        public float OffPeakUpload { get; set; }
    }
}
