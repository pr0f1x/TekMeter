﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TekMeter.Services.TekSavvy
{
    [DataContract]
    class UsageRecordsResponse
    {
        [DataMember]
        public UsageRecordsValue[] value { get; set; }
    }
}
