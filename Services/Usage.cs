﻿using System;

namespace TekMeter
{
    class Usage
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double OnPeakDownload { get; set; }
        public double OnPeakUpload { get; set; }
        public double OffPeakDownload { get; set; }
        public double OffPeakUpload { get; set; }

        public int RoundedOnPeakDownload
        {
            get
            {
                return Convert.ToInt16(Math.Round(OnPeakDownload, MidpointRounding.AwayFromZero));
            }
        }
    }
}
