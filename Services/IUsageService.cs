﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TekMeter.Services
{
    interface IUsageService
    {
        Usage GetUsage();
        bool CanUpdate();
        string GetProviderName();
        IList<UsageHistory> GetUsageHistory();
    }
}
