﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TekMeter.Services;

namespace TekMeter
{
    public partial class History : Form
    {
        public IList<UsageHistory> usageHistoryList;

        public History(IList<UsageHistory> usageHistoryList)
        {
            InitializeComponent();
            this.usageHistoryList = usageHistoryList;
        }

        private void History_Load(object sender, EventArgs e)
        {
            historylistView.Items.Clear();

            int itemCount = 0;
            double totalUsageCount = 0;

            lastXDays.Text = "Last " + usageHistoryList.Count + " Days";
            
            List<UsageHistory> SortedUsageHistoryList = usageHistoryList.OrderByDescending(u => u.Date).ToList();

            foreach ( UsageHistory usageHistory in SortedUsageHistoryList)
            {
                ListViewItem historyListViewItem = new ListViewItem();
                itemCount++;
                totalUsageCount += usageHistory.OnPeakDownload;

                if (itemCount % 2 == 0) {
                    historyListViewItem.BackColor = System.Drawing.Color.Lavender;
                }

                historyListViewItem.Text = usageHistory.Date.ToString("MMMM d yyyy");

                historyListViewItem.SubItems.Add(Math.Round(usageHistory.OnPeakDownload, 2) + " GB");
                historyListViewItem.SubItems.Add(Math.Round(usageHistory.OnPeakUpload, 2) + " GB");

                historyListViewItem.SubItems.Add(Math.Round(usageHistory.OffPeakDownload, 2) + " GB");
                historyListViewItem.SubItems.Add(Math.Round(usageHistory.OffPeakUpload, 2) + " GB");

                historylistView.Items.Add(historyListViewItem);
            }

            totalUsage.Text = "Total Usage: " + Math.Round(totalUsageCount, 2).ToString() + " GB";
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
