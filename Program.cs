﻿using System;
using System.Windows.Forms;

namespace TekMeter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            TekMeter tekMeter = new TekMeter();
            Application.Run(tekMeter);
        }
    }
}
