﻿namespace TekMeter
{
    partial class History
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(History));
            this.historylistView = new System.Windows.Forms.ListView();
            this.dateColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.onPeakDownloadColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.onPeakUploadColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.offPeakDownloadColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.offPeakUploadColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.closeButton = new System.Windows.Forms.Button();
            this.lastXDays = new System.Windows.Forms.Label();
            this.totalUsage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // historylistView
            // 
            this.historylistView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dateColumnHeader,
            this.onPeakDownloadColumnHeader,
            this.onPeakUploadColumnHeader,
            this.offPeakDownloadColumnHeader,
            this.offPeakUploadColumnHeader});
            this.historylistView.Location = new System.Drawing.Point(12, 39);
            this.historylistView.Name = "historylistView";
            this.historylistView.Size = new System.Drawing.Size(510, 384);
            this.historylistView.TabIndex = 0;
            this.historylistView.UseCompatibleStateImageBehavior = false;
            this.historylistView.View = System.Windows.Forms.View.Details;
            // 
            // dateColumnHeader
            // 
            this.dateColumnHeader.Text = "Date";
            this.dateColumnHeader.Width = 124;
            // 
            // onPeakDownloadColumnHeader
            // 
            this.onPeakDownloadColumnHeader.Text = "On Peak D/L";
            this.onPeakDownloadColumnHeader.Width = 87;
            // 
            // onPeakUploadColumnHeader
            // 
            this.onPeakUploadColumnHeader.Text = "On Peak U/L";
            this.onPeakUploadColumnHeader.Width = 86;
            // 
            // offPeakDownloadColumnHeader
            // 
            this.offPeakDownloadColumnHeader.Text = "Off Peak D/L";
            this.offPeakDownloadColumnHeader.Width = 84;
            // 
            // offPeakUploadColumnHeader
            // 
            this.offPeakUploadColumnHeader.Text = "Off Peak U/L";
            this.offPeakUploadColumnHeader.Width = 87;
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(447, 429);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 1;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // lastXDays
            // 
            this.lastXDays.AutoSize = true;
            this.lastXDays.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastXDays.Location = new System.Drawing.Point(13, 13);
            this.lastXDays.Name = "lastXDays";
            this.lastXDays.Size = new System.Drawing.Size(86, 18);
            this.lastXDays.TabIndex = 2;
            this.lastXDays.Text = "Last 0 Days";
            // 
            // totalUsage
            // 
            this.totalUsage.AutoSize = true;
            this.totalUsage.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalUsage.Location = new System.Drawing.Point(13, 429);
            this.totalUsage.Name = "totalUsage";
            this.totalUsage.Size = new System.Drawing.Size(111, 16);
            this.totalUsage.TabIndex = 3;
            this.totalUsage.Text = "Total Usage: 0 GB";
            // 
            // History
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 464);
            this.Controls.Add(this.totalUsage);
            this.Controls.Add(this.lastXDays);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.historylistView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "History";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "History";
            this.Load += new System.EventHandler(this.History_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView historylistView;
        private System.Windows.Forms.ColumnHeader dateColumnHeader;
        private System.Windows.Forms.ColumnHeader onPeakDownloadColumnHeader;
        private System.Windows.Forms.ColumnHeader onPeakUploadColumnHeader;
        private System.Windows.Forms.ColumnHeader offPeakDownloadColumnHeader;
        private System.Windows.Forms.ColumnHeader offPeakUploadColumnHeader;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Label lastXDays;
        private System.Windows.Forms.Label totalUsage;
    }
}