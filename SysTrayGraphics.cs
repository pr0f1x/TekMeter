﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;

/*
 * Based on the open source project CapSavvy by Guzpaz
 */ 
namespace TekMeter
{
    class SysTrayGraphics : IDisposable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass")]
        [DllImport("user32.dll", EntryPoint = "DestroyIcon")]
        static extern bool DestroyIcon(IntPtr hIcon);

        private System.Windows.Forms.NotifyIcon sysTrayIcon;

        private Bitmap bitmap = new Bitmap(16, 16);
        private SolidBrush brush;
        private Font font = new Font(FontFamily.GenericSansSerif, 7);
        private Graphics graphics;

        public Color backColour;
        public Color foreColour;
        public bool transparent = true;

        public SysTrayGraphics(System.Windows.Forms.NotifyIcon sysTrayIcon)
        {
            this.sysTrayIcon = sysTrayIcon;

            graphics = Graphics.FromImage(bitmap);
            graphics.TextRenderingHint = TextRenderingHint.SingleBitPerPixelGridFit;
            brush = new SolidBrush(foreColour);
        }

        private void InitIcon()
        {
            brush.Color = foreColour;
            graphics.Clear(transparent ? Color.Transparent : backColour);
        }

        public void InitializeSysTrayIcon()
        {
            InitIcon();

            graphics.DrawIcon((Icon)(new System.ComponentModel.ComponentResourceManager(typeof(TekMeter))).GetObject("$this.Icon"), new Rectangle(0, 0, 16, 16));

            SetIcon();
        }

        public void SetIcon()
        {
            IntPtr hIcon = bitmap.GetHicon();
            Icon icon = Icon.FromHandle(hIcon);
            sysTrayIcon.Icon = icon;

            // Free icon on Win32, ignore failure on Linux
            try { DestroyIcon(hIcon); } catch { }
        }

        public void UpdateSysTrayIconValues(double first, double second, bool line)
        {
            InitIcon();

            DrawNumbers(first, second);
            if (line) {
                DrawLine(first);
            }

            SetIcon();
        }

        private void DrawLine(double first)
        {

            if (first <= 0 || Properties.Settings.Default.BandwidthLimit <= 0)
            {
                return;
            }

            double percent = Math.Round(first * 100.0 / Properties.Settings.Default.BandwidthLimit, MidpointRounding.AwayFromZero);

            if (percent < 75)
            {
                return;
            }

            Color lineColor = Color.Yellow;

            if ( percent > 90 )
            {
                lineColor = Color.Red;
            }

            Pen pen = new Pen(lineColor);
            graphics.DrawLine(pen, 15, 0, 15, 16);
        }

        private void DrawNumbers(double first, double second)
        {
            if (second < 0)
            {
                DrawNum(0, 5, RoundUp(first, 0));
            }
            else
            {
                DrawNum(0, 0, RoundUp(first, 0));
                DrawNum(0, 9, RoundUp(second, 0));
            }
        }

        private void DrawNum(float x, float y, double num)
        {
            x -= 2; y -= 3;
            graphics.DrawString(num.ToString(), font, brush, x, y);
        }

        double RoundUp(double num, int precision)
        {
            num *= Math.Pow(10, precision);
            num = Math.Ceiling(num);
            num /= Math.Pow(10, precision);
            return num;
        }

        public void Dispose()
        {
            bitmap.Dispose();
            font.Dispose();
            brush.Dispose();
        }
    }
}